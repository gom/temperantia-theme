# Customizing the temperantia GTK theme

**Appx. reading time: 7 min**.  

By design, as it were, the temperantia GTK theme is meant to be as easy as possible to customize, with regards to colors as well as changes that are more in-depth. However, the primary focus of this document is on the former kind of customization. Guides on how to change **the main accent color**, **background colors**, **text colors**, and the **main font weight** can be found further down.  

There isn't a GUI for any of this (but perhaps there will be in the future), instead you have to edit the CSS files to accomplish the changes you need or want. However, as alluded to earlier, these files are made to be easy to understand and navigate. For example, in the file where you can edit all colors, each section has a heading and a short description of what the colors in question are used for.  

Before you start doing your customizations, it's highly recommended that you read the sections below about making a **backup** and about the **structure** of the theme and its files. If you feel that's overwhelming, **at least** read the paragraph right below here, about the importance of having a **backup**.  

## IMPORTANT: Make a backup

If you want to do **any** kind of customization of the temperantia GTK theme, it's **strongly recommended** that you first **make a backup** of the original folders and files. This will save you both time and pain if you, say, change a color and later want to go back to what is was originally but can't remember what it was. Moreover, and concerning more of in-depth customizing, it's easier than one might think to write CSS code that break GTK applications, and the process of finding one's way back to a working stylesheet isn't always that straightforward. Having a backup in such situations is, of course, of outmost importance.  

## Overview & structure

All files containing the various stylings can be found within the directory called `gtk-3.0`, which is a subdirectory of both `temperantia` (main directory of light variant), and `temperantia-dark` (main directory of dark variant). There are quite a few files within the `gtk-3.0` directory, and the reason for this is that the temperantia GTK theme isn't contained within a single CSS file, which is usually the case. Instead it's spread out over several files, which are connected by virtue of the `@import` rule. The `gtk.css` and `gtk-dark.css` files actually don't contain any stylings at all, only several `@import` rules which pull in all of the necessary CSS files:  

* `1_main.css`  
* `2_entries-buttons.css`  
* `3_scrollbars-switches-checks-radios.css`  
* `4_scale-progress-level.css`  
* `5_applications.css`  
* `6_terminal.css`  
* `colors-light.css` **or** `colors-dark.css`  
* `colors-palette.css`  
* `titlebuttons-light.css` **or** `titlebuttons-dark.css`  

The file `1_main.css` contains stylings for basic widgets like windows, toolbars, headerbars, menus, lists, rows, etc. The contents of the other CSS files are probably easier to figure out due to how they are named. In any case, for the purpose of doing color customizations, **three files** are of **specific interest**:  

* `colors-light.css`  
* `colors-dark.css`  
* `colors-palette.css`  

`colors-light.css` contains *all* colors that are used in the light variant, and `colors-dark.css` contains *all* colors that are used in the dark variant. Do **note**, though, that `colors-dark.css` is used by *both* the light and the dark variant, in order for applications to be able to use a "dark mode" even when the light variant of the theme is active. Importantly, this means that if you **edit any of the two colors-dark files**, you want to make sure they stay identical. One way of doing that, is to simply copy the colors-dark file you've edited, then paste it into the folder where the other one is located, in order for the latter to be overwritten.  

Now, there aren't any *actual* colors in either of these files, only color *names*, referring to color definitions made in `colors-palette.css`. This file contains the whole **temperantia color palette**, which means it consists of names and corresponding definitions of the colors that are used in `colors-light.css` and `colors-dark.css`. This file is the same for both the light and dark variant.  

To further clarify, a color definition in `colors-palette.css` looks e.g. like this:  

```
@define-color rose_500 rgb(204, 100, 138);  
```
What this essentially does, is to define a color for a specific name, which in this case means the color expressed as an RGB triplet above is defined for `rose_500`. This makes it possible to *refer* to this color by using just the name, prefixed by an **at sign** (@). And that's what's happening in `colors-light.css` and `colors-dark.css`. For example, it might look like this:  

```
@define-color accent_main @rose_500;  
```

Here, then, `rose_500` is used to define the same color yet again, but for another name - `accent_main`. In this case, then, both `rose_500` and `accent_main` refer to the same color. While this might seem like an unnecessarily complex way of working, it makes it possible to use, in this particular case, `accent_main` in the stylesheet instead of `rose_500`. In turn, this makes customization a lot easier, which of course is the whole point, since changing a color only requires changing one color definition, instead of changing all the various instances of it throughout the stylesheet.  

# Four examples of customizations

Customizing a color in the temperantia theme entails making edits to one or both of the files `colors-light.css` and `colors-dark.css`, depending on whether you want to customize both or just one of the theme variants. For the sake of comprehensibility, in the below examples of color customizations, these will be applied to the **light** variant only, i.e. the `colors-light.css` file. The general procedure is basically the same, though, regardless of what variant you're customizing.  

More specifically, changing a color is a matter of replacing a color *name* that looks e.g. like this: `@rose_500`. If using a color from the **temperantia color palette**, it needs to **start with an at sign** (@), in order for the actual color to be found in `colors-palette.css`. All colors in the palette also come in various shades, indicated by their numerical values - a higher value means a darker shade, a lower one a brighter shade.  

Besides using colors from the temperantia color palette, colors can also be specified in other ways:  

- Using an **RGB triplet**, e.g. `rgb(0, 128, 128)`  
- Using **HEX color code**, e.g. `#008080`  
- Using a **standardized color name**, e.g. `teal`  

## Main accent color

**1.** If you haven't already, open the `colors-light.css` file. The section where accent colors are defined starts at `line 18` (keep in mind, though, that this might change, albeit slightly, in future updates). The first line right below the subheading **MAIN accent color**, is the one to edit in order to have another accent color than the default rose color.  

**2.** If you want to use a color from the **temperantia color palette**, open `colors-palette.css`, if you haven't already. This makes it a bit easier to copy and paste the color names, e.g. if trying out several of them. It's recommended to use either a **500** or a **900** shade of any of the following colors:  

* `dawn` (blue)  
* `overcast` (gray)  
* `ocean` (green-blue)  
* `sea` (blue-green)  
* `bark` (brown)  
* `grapefruit` (light red)  
* `orchid` (magenta)  
* `amethyst` (purple)  

The other accent colors in the palette should *not* be utilized as the *main* accent color, since they are used as e.g error, warning and success colors. However, while `dusk` (violet) certainly *could* be used as the main accent, if doing so, the **Alternative accent color** needs to be changed too, since by default that's defined to be `dusk`.  

**3.** Using `sea_500` as an example, a change of the default main accent color would look like this:  

```
@define-color accent_main @sea_500;  
```

**4.** Using a color that's *not* in the temperantia palette, expressed as an **RGB triplet**, could look like this:  

```
@define-color accent_main rgb(144, 137, 116);  
```

Using the same color, but as **HEX color code**, would look like this:  

```
@define-color accent_main #908974;  
```

## Background colors

**1.** If you haven't already, open the `colors-light.css` file. The section where background colors are defined starts at `line 103` (keep in mind, though, that this might change, albeit slightly, in future updates).  

When changing the background colors, it's recommended to at least have as a starting point to maintain the **relative** brightness/darkness between the main background colors. Going from brightest to darkest, they are the following:  

- `bg_txt`  
- `bg_1`  
- `bg_2`  
- `bg_3`  

> **NOTE**: In the **dark** variant, `bg_txt` is the *darkest* background color, otherwise the same principles applies  

**2.** So, let's say you'd want to make the light variant of the theme overall brighter. In such a case you can start by looking in `colors-palette.css` to find out what color each of the above refer to. In the case of `bg_txt`, for example, the color name `cloud_100` is used, and looking in the palette that's the following color: `rgb(245, 244, 246)`. There aren't any brighter shades of `cloud` in the palette, but you can manually make a brighter version of it by increasing each numerical value equally much. Increasing every value by 5, for example, would generate the following color: `rgb(250, 249, 251)`. For `bg_1`, `bg_2` and `bg_3`, brighter shades of the different `cloud` variants they use do exist, which they can be changed to.  

Replacing these four background colors with brighter ones, then, could look something like this:  

```
@define-color bg_1 @cloud_100;  
@define-color bg_2 @cloud_500;  
@define-color bg_3 @cloud_700;  
@define-color bg_txt rgb(250, 249, 251);  
```

Of course, the background colors can also be replaced by colors not existing in the palette. Keeping in mind that `bg_txt` should be the brightest of the main background colors, it could look something like this using only **HEX color codes**:  

```
@define-color bg_1 #f5f5f5;  
@define-color bg_2 #e4e4e4;  
@define-color bg_3 #cdcdcd;  
@define-color bg_txt #ffffff;  
```

## Text colors

**1.** If you haven't already, open the file `colors-light.css`. The section where text colors are defined starts at `line 148` (keep in mind, though, that this might change, albeit slightly, in future updates).  

If you're goal, for example, is to overall darken the text color, the recommendation is to start by changing the colors for `txt_main` and `txt_main_2`, which are used for most text and foregrounds. When you're satisfied with those, then move on with the other text colors.  

**2.** `txt_main` and `txt_main_2` are both using the color called `night` in the palette - the latter a slightly darker shade. To keep using `night`, but darker variants of it, could for example look like this:  

```
@define-color txt_main @night_700;  
@define-color txt_main_2 @night_900;  
```

To have even darker colors, you could try using the `dirt` color from the palette, like so:  

```
@define-color txt_main @dirt_700;  
@define-color txt_main_2 @dirt_900;  
```

Naturally, you can also try using any other color, for example a standardized color name like `black`, for both `txt_main` and `txt_main_2`:  

```
@define-color txt_main black;  
@define-color txt_main_2 black;  
```

## Main font weight

By default, the main font weight is defined with a value of `600`, which is equivalent to `semi-bold`. There are a few reasons for this, but mainly it's a question of overall legibility. However, if you'd like to have a thinner weight, that's relatively easy to achieve.  

**1.** Start by opening the file `1_main.css`. Do a search for the phrase "main font weight", which should take you to the right place. Below several selectors, the first one being `.background`, you should see a line that says `font-weight: 600`. That's the value to change.  

**2.** For defining a thinner weight, there are a few different options. A value of `500` will give you a weight equivalent to `medium`. If you want to have the basic, most commonly used weight for regular text, you can use either `400` or `normal`.  

Using a value of `500` as an example, the change would look like this:  

```
font-weight: 500;  
```

**3.** You might also want to apply the same change to the **GNOME Shell theme**, since its default main font weight is also defined with a value of `600`. To do this, navigate to the folder `temperantia-shell/gnome-shell`, and open the file called `gnome-shell.css`.  

**4.** The main font weight for the GNOME Shell theme is defined right below the very first heading, **GLOBALS**. Change it in the same way the main font weight for the GTK theme was changed, which in this example would be:  

```
font-weight: 500;
```

**5.** For the change to take effect, however, the GNOME Shell theme needs to be reloaded. You can do this by, first, pressing <kbd>Alt</kbd> + <kbd>F2</kbd> to launch the **run dialog**, then typing `rt` in the dialog's entry, and, lastly, pressing <kbd>Enter</kbd>.  



