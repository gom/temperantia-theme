# Installation of GDM3 theme (login and unlocking screens)

**Appx. reading time: 3 minutes**  

In order to install the temperantia GDM3 theme, the temperantia GNOME Shell theme needs to be made the *default*. This means replacing the current default GNOME Shell theme, which is probably Adwaita - if you aren't using another theme as your default. The commands for doing this are based on the GDM3 install instructions that come with the [Materia theme](https://github.com/nana-4/materia-theme/ "https://github.com/nana-4/materia-theme/"). All of them have recently (June, 2022) been tested on fresh installs of Debian 11 on several devices without issues. A few words of **caution**, though:  

* Replacing the default GNOME Shell theme is **not entirely without risks**. Issues might arise *if* the replacement procedure for some reason fails. The instructions below, however, include making a backup of the current default GNOME Shell theme, which should at least be helpful in solving or mitigating such mishaps.  

* If using any other GNOME Shell theme *after* the replacement's been done, these will look more or less broken. You'll need to revert back to the original default theme, in order for other GNOME Shell themes to look as intended. Instructions for doing so are also included below.  

* When GNOME Shell receives **updates**, it automatically makes the original theme the default one. In such instances, therefore, you need to re-install the temperantia GDM3 theme, by repeating steps **3.** to **5.** below.  

## Avatar alternatives

Before installing the temperantia GDM3 theme, you may want to choose a specific color variant of the **avatar icon** that is shown during login and when unlocking the screen.  

The default avatar is colored using the rose (pink) color from the temperantia palette. To browse other color variants, navigate to the directory `temperantia-shell/gnome-shell/graphics`. There you'll find a file named `avatar.png`, as well as a directory named `avatar-alternatives`. Within the latter, 13 additional color options of the avatar icon can be found, alongside the default rose colored one. If you want to use any of these alternatives, **copy** it over to the `graphics` directory, **remove** the current default `avatar.png`, and then **rename** the new icon `avatar.png`  

## Installation preparations

**1.** First, you need to install the required package `glib-compile-resources`. Depending on the distro, this goes by a few different names:  

- **Arch/Arch based** --> `glib2`  
- **Debian**/**Debian based** --> `libglib2.0-dev-bin`  
- **Fedora**, **openSUSE**, et al. --> `glib2-devel`  

> NOTE: **If you're using Ubuntu**, besides installing `libglib2.0-dev-bin`, you also need to install `libxml2-utils`  

**2.** Then, if you haven't already, you also need to copy the `temperantia-shell` folder to the root `themes` directory. It is also recommended to copy the **temperantia GTK theme** folders to this directory, which is what the below command will do. Open a terminal in the directory called `temperantia-theme`, and run the following:  

```
sudo cp -r temperantia/ temperantia-dark/ temperantia-shell/ /usr/share/themes  
```

## Installation

**1.** Before the actual replacement/installation, you need to create a **backup** of the current default GNOME Shell theme. This is done by adding a [tilde](https://en.wikipedia.org/wiki/Tilde#Backup_filenames "https://en.wikipedia.org/wiki/Tilde#Backup_filenames") suffix to a copy of the file that contains the compiled resources of this theme. The tilde suffix will also make the copy of the file hidden. So, to backup the current default GNOME Shell theme, run the following command:  

```
sudo cp -av /usr/share/gnome-shell/gnome-shell-theme.gresource{,~}  
```

**2.** To make **temperantia** the **new default** GNOME Shell theme, the various resources of the temperantia theme (CSS files, image files, etc.) need to be compiled into a file, which will replace the corresponding file of the current default theme. This is done by running the following:  

```
sudo glib-compile-resources --target="/usr/share/gnome-shell/gnome-shell-theme.gresource" --sourcedir="/usr/share/themes/temperantia-shell/gnome-shell" "/usr/share/themes/temperantia-shell/gnome-shell/gnome-shell-theme.gresource.xml"  
```

**3.** The temperantia GDM3 theme should now have been **installed**. For the changes to take effect, however, the theme should be reloaded. Do this by, first, pressing <kbd>Alt</kbd> + <kbd>F2</kbd> to launch the **run dialog**, then typing `rt` in the dialog's entry, and, lastly, pressing <kbd>Enter</kbd>. To make sure everything has worked as intended, you can try locking and unlocking the screen. Possibly, you may need to log out and log in, **or** reboot, for *all* changes to take effect.  

**4.** As a final touch, you may want to fine-tune the look of the login screen, e.g. what font or cursor theme to use. This can be configured in the `greeter.dconf-defaults` file, which can be edited using e.g. nano or Gedit. If you're not comfortable editing within a terminal, I recommend using the latter - simply replace `nano` with `gedit` in the below command:  

```
sudo nano /etc/gdm3/greeter.dconf-defaults  
```

Now, let's say that for you're login screen you want to use the FreeSans font, size 12, the cherry colored Oxygen cursor theme, and the temperantia icon theme (rose colored variant). To achieve this, a few lines have to be added, below `[org/gnome/desktop/interface]`. If you're `greeter.dconf-defaults` file already contains configurations for, in this case, font, cursor theme, and icon theme, these can be commented out by placing a `#` at the beginning of the lines. Alternatively, you can remove them altogether, though as a precaution it's probably better to just comment them out.  

To make the changes specified above, it should look something like this:  

```
[org/gnome/desktop/interface]  
font-name='FreeSans 12'  
cursor-theme='oxy-cherry'  
icon-theme='temperantia-rose'  
```

> TIP: **dconf Editor** is very helpful in making sure one's configurations are written correctly. For example it should be `oxy-cherry`, in lowercase, **not** `Oxy-cherry`.  

When you're done with your configurations, save the file, and reboot your device if you want to scrutinize your changes right away.  

# Uninstallation

To make the original GNOME Shell theme the default again, run the following command to rename the previously made backup file, which will then overwrite the current default, effectively uninstalling the temperantia GDM3 theme:  

```
sudo mv -v /usr/share/gnome-shell/gnome-shell-theme.gresource{~,}  
```

