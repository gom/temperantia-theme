# temperantia GTK & GNOME Shell themes

**Appx. reading time: 4 minutes**  

temperantia is a theme for the GNOME desktop environment, focused on **customizability**, **accessibility**, and **inclusion**. Neither the GTK nor the GNOME Shell theme are based on any other theme in particular, but have been written from the ground up, basically. It's been in development since early 2020, and the first version was released in June 2022.  

# Compatibility

* temperantia is compatible *primarily* with **GNOME v. 3.38** - it's been developed, and also tested, mostly under [Debian 11](https://www.debian.org/News/2021/20210814 "https://www.debian.org/News/2021/20210814") (a.k.a. *Bullseye*), but will probably work well on other distros, as long as the version of GNOME is compatible (v. 3.36 *might* work)  

* GTK2 is **not** supported  

* GTK4 isn't **fully** supported - many GTK4 applications seem to mostly work fine, though  

# Some features & characteristics

* By default, and overall, the focus of the design is **clarity** and **minimalism**, while at the same time being **inspiring** and (to some degree) **colorful**  

* Pure **black** and **white** colors are practically non-existent, and in general the **colors** are somewhat **muted**, or **desaturated** - a great deal of time and energy have been put in to make this work without the sacrifice of decent **contrasts** and **legibility**   

* The GTK theme, in particular, is highly **customizable** - for instance, any color can be quite easily changed, see [`CUSTOMIZING.md`](CUSTOMIZING.md)  

* The GTK as well as the GNOME Shell theme **scale** with the font size, which, among other things, make them suitable for use on e.g. **tablets**  

* The GNOME Shell theme includes a theme for **GDM3** (login and unlocking screens), see [`GDM3-INSTALLATION.md`](GDM3-INSTALLATION.md)  

* Some **windows** (e.g. those without [client side decorations](https://en.wikipedia.org/wiki/Client-Side_Decoration "https://en.wikipedia.org/wiki/Client-Side_Decoration")) are **uniformly colored**, i.e. their title bars have the same color as the rest of the window. Partly, this is done to make title bars fit better with Qt applications, and partly because non-CSD GTK applications' title bars tend to be quite short, creating too much of visual inconsistency if colored. Such a design choice might seem like it makes it harder to grab and drag windows of the latter kind. But keep in mind that all menubars are draggable, and since most non-CSD GTK applications *have* a menubar, their draggable space is quite large.  

* **Dialog windows** are nearly completely draggable, i.e. they can be grabbed almost anywhere on their surface  

# Previews

![Screenshot of the GNOME desktop, using light variant of the temperantia GTK theme, and the temperantia GNOME Shell theme](images/temperantia-light-full.png)  

![Screenshot of the GNOME desktop, using dark variant of the temperantia GTK theme, and the temperantia GNOME Shell theme](images/temperantia-dark-full.png)  

![Screenshot of the GNOME login dialog using the temperantia GDM3 theme](images/temperantia-login.png)  

# Installation

There are basically three levels of integration of the temperantia theme in the GNOME desktop environment:  

**1**) Installation at local user level  

**2**) Installation at system wide level  

**3**) Option 2) **plus** installation of the GDM3 theme (login and unlocking screens)  

The most visually consistent experience will be achieved by installing the themes at system wide level **and** installing the GDM3 theme. Instructions for local and system wide level installation can be found below. For installing the GDM3 theme, see [`GDM3-INSTALLATION.md`](GDM3-INSTALLATION.md).  

## Installation at local user level

**1.** To install for a local user, first navigate to the directory named `temperantia-theme`, inside of which the three folders named `temperantia`, `temperantia-dark`, and `temperantia-shell` are located.  

**2.** Open a terminal here, and then run the below command to, firstly, create the hidden directory `.themes` in your home folder, and then copy the aforementioned three folders to this directory (if the directory already exists, the folders will just be copied over to it):  

```
mkdir ~/.themes; cp -r temperantia/ temperantia-dark/ temperantia-shell/ ~/.themes  
```

## Installation at system wide level

**1.** To install for system wide use, first navigate to the directory named `temperantia-theme`, inside of which the three folders named `temperantia`, `temperantia-dark`, and `temperantia-shell` are located.  

**2.** Open a terminal here, and then run the below command to copy the aforementioned three folders to the root `themes` directory:  

```
sudo cp -r temperantia/ temperantia-dark/ temperantia-shell/ /usr/share/themes  
```

# Background

At its heart, temperantia is a project concerned with the importance of customizability of desktop environments, and in particular its main theme. It's very common, however, to frame this solely in terms of "aesthetic preferences" or "looks". But that's reductive – and sometimes, frankly, even disrespectful. Because to me, it's clearly the case that, besides being about beauty, customizability is closely connected to matters such as accessibility and inclusion.  

I'm neurodivergent, and more specifically I'm autistic and also have ADHD (inattentive type). Among other things, this means I’m highly sensitive to various sensory input, including colors, and I also get distracted very easily. I can have a hard time even using a DE (or any UI) because I get so mentally and/or physically affected by the color choices and color combinations, or because text is too small or too unclear for me to quickly and easily perceive. For a DE to be usuable to me, and especially GNOME, I need to be able to adjust various aspects *in detail*.  

Further, while my birth sex is male, my gender identity and expressions have always been fluid and shifting. Most things related to masculinity makes me, at best, very uncomfortable, and can sometimes also make me feel unsafe. Many of the desktop environments I've used remind me, to varying degrees, of masculine settings and/or masculinity in general. The standardized blue accent color is just *one* example. I don't feel comfortable with that, at all. In order to feel comfortable and acknowledged in the DE where I live, I need to be able to use *specific* accent colors - and colors in general - of my own choosing.  

On a personal level, as it were, these are my reasons for creating temperantia, and especially the temperantia GTK and GNOME Shell themes. Then again, one is never *just* a person, and I know that at least some people feel like I do when it comes to customization in relation to various needs. My hope, then, in general is that temperantia can be of **some** help, in **some** way.  

# Acknowledgements

* [KDE's Oxygen theme](https://invent.kde.org/plasma/oxygen "https://invent.kde.org/plasma/oxygen") has been an important source of inspiration for the whole design of the GTK theme - although it might not be that apparent, other than in the design of the titlebuttons (which also draws some inspiration from the Breeze theme)  

* [elementary's](https://elementary.io/ "https://elementary.io/") GTK stylesheets have been very helpful in solving various issues, especially regarding the styling of scales, progressbars, and levelbars  

* Some design aspects of the top panel are inspired by the [Orchis GNOME Shell theme](https://github.com/vinceliuice/Orchis-theme "https://github.com/vinceliuice/Orchis-theme")  

* Thanks to my beautiful friends in the fediverse, especially [grey](https://adikos.net "https://adikos.net"), for their invaluable support  

* And, lastly, thanks to everyone working to make FOSS DEs customizable, accessible, and inclusive  